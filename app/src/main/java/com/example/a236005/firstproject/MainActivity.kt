package com.example.a236005.firstproject

import android.app.ActionBar
import android.os.Bundle
import android.content.Intent
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var toolbar: ActionBar

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_Balances -> {

                return@OnNavigationItemSelectedListener true
             }
            R.id.nav_Forex -> {

                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_EasyPay -> {
                showScreen()
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_Contact -> {

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun showScreen() {
        startActivity(Intent(this, EasyPayOptionsActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        supportActionBar?.hide()
//        findViewById<Button>(R.id.helloButton).setOnClickListener { showSecond() }
    }
}
