package com.example.a236005.firstproject.Payment

import android.icu.util.CurrencyAmount
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.a236005.firstproject.R
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.activity_payment.view.*

class Payment : AppCompatActivity(), PayView {
    override fun onPaymentSuccess() {
 //Show success toast
    }

    override fun onfail(errorMessage: String) {
   //Show error toast
    }

    private lateinit var Paybtn: Button
    private lateinit var eAmount : EditText

    private lateinit var viewModel: PayViewModel
    private lateinit var myUserId: String
    private lateinit var recipientUserId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        myUserId = intent.getStringExtra("MY_USER_ID")
        recipientUserId = intent.getStringExtra("RECIPIENT_USER_ID")
       viewModel= PayViewModel(this)
       Paybtn= findViewById(R.id.PayBtn)
        eAmount=findViewById(R.id.Amount)

        Paybtn.setOnClickListener { viewModel.initialPayment(eAmount.text.toString(),myUserId, "rUserID") }


    }
}
