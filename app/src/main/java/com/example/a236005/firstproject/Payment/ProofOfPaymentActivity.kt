package com.example.a236005.firstproject.Payment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.a236005.firstproject.R
import java.io.File
import java.io.FileOutputStream

class ProofOfPaymentActivity : AppCompatActivity() {
    lateinit var preView : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_proof_of_payment)
        val intent = intent

        preView = findViewById(R.id.content)

        findViewById<TextView>(R.id.ref).text = intent?.extras?.getString("your_ref")
        findViewById<TextView>(R.id.acc).text = intent?.extras?.getString("acc_num")
        findViewById<TextView>(R.id.amount_txt).text = intent?.extras?.getString("amount")
        findViewById<TextView>(R.id.their_ref).text = "Nompumelelo"

        findViewById<ImageView>(R.id.share).setOnClickListener {
            takeScreenShotAndShare(this, preView, true, "Receipt")
        }

    }

    override fun onResume() {
        super.onResume()

        supportActionBar?.title = "Payment Successful"
    }

    fun takeScreenShotAndShare(context: Context, view: View, incText: Boolean, text: String) {

        try {
            val mPath = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "screenshot.png")

            view.isDrawingCacheEnabled = true
            val bitmap = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false

            val fOut = FileOutputStream(mPath)
            val quality = 100
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, fOut)
            fOut.flush()
            fOut.close()

            val shareIntent = Intent(Intent.ACTION_SEND)
            val pictureUri = Uri.fromFile(mPath)
            shareIntent.type = "image/*"
            if (incText) {
                shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            }
            shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            context.startActivity(Intent.createChooser(shareIntent, "Share image using"))
        } catch (tr: Throwable) {
            Log.d("ProofOfPaymentActivity", "Couldn't save screenshot", tr)
        }

    }
}
