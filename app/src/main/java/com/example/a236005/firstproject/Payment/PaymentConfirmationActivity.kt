package com.example.a236005.firstproject.Payment

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.a236005.firstproject.R

class PaymentConfirmationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_confirmation)

        val intent = intent

        val userAccount = intent?.extras?.getString("user_details")
        val userAccountArray = userAccount?.split(", ")

        val firstName = findViewById<TextView>(R.id.firstName)
        val name = userAccountArray!![0].split("=")
        firstName.append(name[1])

        val surname = findViewById<TextView>(R.id.surname)
        val lastName = userAccountArray[1].split("=")
        surname.append(lastName[1])


        val accountNumber = findViewById<TextView>(R.id.accountNumber)
        val accNum = userAccountArray[2].split("=")
        accountNumber.append(accNum[1])

        val accountType = findViewById<TextView>(R.id.accountType)
        val accType = userAccountArray[3].split("=")
        accountType.append(accType[1])

        val amount = findViewById<EditText>(R.id.input_amount).text.toString().trim()

        findViewById<Button>(R.id.confirmPayment).setOnClickListener {
            processPayment(amount, name[1], accNum[1])
        }

    }

    override fun onResume() {
        super.onResume()

        supportActionBar?.title = "Confirm Payment"
    }

    private fun processPayment(amount: String, yourRef: String, accNum: String) {
        val intent = Intent(this, ProofOfPaymentActivity::class.java)

        val bundle = Bundle()
        bundle.putString("your_ref", yourRef)
        bundle.putString("acc_num", accNum)
        bundle.putString("amount", amount)

        intent.putExtras(bundle)
        startActivity(intent)
    }
}
