package com.example.a236005.firstproject

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import com.example.a236005.firstproject.permissions.PermissionsActivity
import com.example.a236005.firstproject.qrcode.GenerateQRCodeActivity
import com.example.a236005.firstproject.qrcode.ScanQRCodeActivity
import kotlinx.android.synthetic.main.easy_pay_options_activity.*

class EasyPayOptionsActivity : PermissionsActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.easy_pay_options_activity)

        val scanButton = findViewById<LinearLayout>(R.id.ScanLayout)
        scanButton.setOnClickListener { showScanner() }

      val generateButton = findViewById<LinearLayout>(R.id.generateQRCodeLayout)
        generateButton.setOnClickListener { showGenerateQRCodeScreen() }
    }

    private fun showGenerateQRCodeScreen() {
        val intent= Intent(this, GenerateQRCodeActivity()::class.java)
       startActivity(intent)
    }

    private fun showScanner() {
        requestPermissions(Manifest.permission.CAMERA)
    }

    override fun permissionGranted(permission: String) {
        val intent = Intent(this, ScanQRCodeActivity()::class.java)
        startActivity(intent)
    }

    override fun permissionDenied(permission: String) {

    }
}
