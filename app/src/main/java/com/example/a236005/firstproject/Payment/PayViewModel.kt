package com.example.a236005.firstproject.Payment

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException

class PayViewModel(private val view: PayView, private val firebaseDb: FirebaseFirestore = FirebaseFirestore.getInstance()) {
    fun initialPayment(amount: String, myuserID: String, recipientUserID: String) {
        val myAccount = firebaseDb.collection("Account").document(myuserID)
        val recipientAccount = firebaseDb.collection("Account").document(recipientUserID)
        firebaseDb.runTransaction { transaction ->

            val snapShot = transaction.get(myAccount)
            val recipientSnapshot = transaction.get(recipientAccount)

            val myBalance = snapShot.getDouble("available_balance")
            val recipientBalance = recipientSnapshot.getDouble("available_balance")
            val paymentAmount = amount.toDouble()

            if (myBalance != null) {
                if (myBalance < paymentAmount) {
                    val errorMessage = "You have insufficient funds to make this transaction"
                    throw FirebaseFirestoreException(
                        errorMessage,
                        FirebaseFirestoreException.Code.ABORTED
                    )

                }
                if(recipientBalance == null) {
                    val errorMessage = "Recipient doesn't have an account"

                    throw FirebaseFirestoreException(errorMessage,
                    FirebaseFirestoreException.Code.ABORTED)
                }
                val recipientNewBalance = recipientBalance + paymentAmount
                val myNewBalance = myBalance - paymentAmount

                transaction.update(myAccount, "available_balance", myNewBalance)
                transaction.update(recipientAccount, "available_balance", recipientNewBalance)

            }

        }.addOnSuccessListener { result ->
            view.onPaymentSuccess()
        }.addOnFailureListener { result ->
            view.onfail(result.message.toString())
        }
    }
}


