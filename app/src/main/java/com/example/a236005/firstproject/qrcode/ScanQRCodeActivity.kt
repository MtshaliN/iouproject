package com.example.a236005.firstproject.qrcode

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.example.a236005.firstproject.Payment.PaymentConfirmationActivity
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_second.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScanQRCodeActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    override fun handleResult(result:   Result?) {

        val userDetails = result.toString().substring(16, result.toString().length - 1)
        val bundle = Bundle()
        bundle.putString("user_details", userDetails)
        val intent = Intent(this, PaymentConfirmationActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private var scannerView: ZXingScannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerView = ZXingScannerView(this)
        setContentView(scannerView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //  PaymentBtn.setOnClickListener {ShowPayment()}
    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.title = "Scan QR Code"
        scannerView?.setResultHandler(this)
        scannerView?.startCamera()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


}
